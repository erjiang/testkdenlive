import subprocess
import time

import pyatspi

ACTION_WAIT = 0.1

# https://www.freedesktop.org/wiki/Accessibility/PyAtSpi2Example/
# https://lazka.github.io/pgi-docs/Atspi-2.0/index.html

def get_app_by_name(name):
    desktop = pyatspi.Registry.getDesktop(0)
    for app in desktop:
        if app.name == name and app.getChildCount() > 0:
            print("Found %s" % name)
            return app

def get_app_by_pid(pid):
    desktop = pyatspi.Registry.getDesktop(0)
    for app in desktop:
        if app.get_process_id() == pid:
            return app

def get_biggest_frame(app):
    frames = [f for f in app if f.getRole() == pyatspi.ROLE_FRAME]
    if not frames:
        return None
    frames.sort(key=lambda f: f.get_size().x * f.get_size().y, reverse=True)
    return frames[0]

def get_child_by_name(obj, name: str, partial_match=False):
    for o in obj:
        if o.name == name or (partial_match and name in o.name):
            return o

def get_children_by_role(obj, role):
    return [o for o in obj if o.getRole() == role]

def get_main_menu(frame):
    for o in frame:
        if o.getRole() == pyatspi.ROLE_MENU_BAR:
            return o

def select_menu_item(frame, items, partial_match=False):
    menu = get_main_menu(frame)
    for i, item in enumerate(items):
        for o in menu:
            if o.name == item or (partial_match and item in o.name):
                print("Clicking on %s" % item)
                click_at(get_obj_center(o))
                print("Doing sleep")
                time.sleep(ACTION_WAIT)
                print("Checking if at end of list")
                if i < len(items) - 1:
                    menu = o[0]
                break
        else:
            print("Menu options are:")
            for o in menu:
                print("* " + o.name)
            raise Exception("Menu item not found: %s" % item)

def list_apps():
    desktop = pyatspi.Registry.getDesktop(0)
    for app in desktop:
        print(repr(app.name))
        if app.name == "kdenlive":
            print("Found it")
            for o in app:
                print(o)

def get_obj_center(obj):
    extents = obj.get_extents(pyatspi.Atspi.CoordType.SCREEN)
    return (extents.x + (extents.width / 2), extents.y + (extents.height / 2))

def click_at(coords):
    x, y = coords
    print("Clicking at %s, %s" % (x, y))
    pyatspi.Registry.generateMouseEvent(x, y, "b1c")

def bring_to_front(obj):
    title = obj.name
    # run wmctrl -R "title" to bring to front
    print("Running wmctrl -R %s" % title)
    subprocess.run(["wmctrl", "-R", title])
    time.sleep(ACTION_WAIT)

def main():
    app = get_app_by_name("kdenlive")
    print(app)
    frame = get_biggest_frame(app)
    bring_to_front(frame)
    print(frame)
    select_menu_item(frame, ["File", "OpenTimelineIO Export"], partial_match=True)
    time.sleep(.3)
    print("Ready to search")
    if get_child_by_name(app, "OpenTimelineIO", partial_match=True):
        print("Found it, seems like OpenTimelineIO export failed")
    for o in app:
        print(o)


if __name__ == "__main__":
    main()
